<?php
/*
 * References
 * - https://tt-rss.org/wiki/MakingPlugins
 * - https://tt-rss.org/wiki/Plugins
 */
class FetchFeed_mTLS extends Plugin {
    private $config;

    function about() {
        return array(
            '0.1',  // version
            'Fetch feeds using mTLS',  // description
            'yan12125',  // author
            true  // is_system
        );
    }

    function init($host) {
        $host->add_hook($host::HOOK_FETCH_FEED, $this);
        $host->add_hook($host::HOOK_SUBSCRIBE_FEED, $this);
        $host->add_hook($host::HOOK_FEED_BASIC_INFO, $this); // Used for retrieving basic info after subscription

        $this->config = array();
        $config_str = getenv('TTRSS_FETCHFEED_MTLS_CONFIG');
        if ($config_str) {
            foreach(explode(';', $config_str) as $item) {
                list($url_prefix, $cert_path) = explode('=', $item, 2);
                $this->config[$url_prefix] = $cert_path;
            }
        }
    }

    function fetch_feed($feed_data, $fetch_url) {
        foreach ($this->config as $url_prefix => $cert_spec) {
            if (substr($fetch_url, 0, strlen($url_prefix)) == $url_prefix) {
                list($cert_path, $key_path) = explode(',', $cert_spec);
                Debug::log("Using client certificate {$cert_path} with key {$key_path}.");
                $ch = curl_init($fetch_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERAGENT, Config::get_user_agent());
                curl_setopt($ch, CURLOPT_SSLCERT, $cert_path);
                curl_setopt($ch, CURLOPT_SSLKEY, $key_path);
                $feed_data = curl_exec($ch);
                curl_close($ch);
                break;
            }
        }
        return $feed_data;
    }

    function hook_fetch_feed($feed_data, $fetch_url, $owner_uid, $feed, $last_article_timestamp, $auth_login, $auth_pass) {
        return $this->fetch_feed($feed_data, $fetch_url);
    }

    function hook_subscribe_feed($contents, $url, $auth_login, $auth_pass) {
        return $this->fetch_feed($contents, $url);
    }

    // Mostly copied from RSSUtils::update_basic_info()
    // https://dev.tt-rss.org/fox/tt-rss/src/commit/ed2cbeffcc456a86726b52d37c977a35b895968c/classes/rssutils.php#L281
    function hook_feed_basic_info($basic_info, $fetch_url, $owner_uid, $feed_id, $auth_login, $auth_pass) {
        $feed_data = $this->fetch_feed(null, $fetch_url);
        if (empty($feed_data)) {
            // Return null to let the original procedure run
            return null;
        }
        $feed_data = trim($feed_data);
        $rss = new FeedParser($feed_data);
        $rss->init();
        if (!$rss->error()) {
            return [
                'title' => mb_substr(clean($rss->get_title()), 0, 199),
                'site_url' => mb_substr(UrlHelper::rewrite_relative($fetch_url, clean($rss->get_link())), 0, 245),
            ];
        }
        return [
            'title' => null,
            'site_url' => null,
        ];
    }

    function api_version() {
        return 2;
    }
}
